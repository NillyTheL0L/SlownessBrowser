from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtPrintSupport import *
import os
import sys

class MainWindow(QMainWindow):

	def __init__(self, *args, **kwargs):
		super(MainWindow, self).__init__(*args, **kwargs)


		self.browser = QWebEngineView()
		self.browser.setUrl(QUrl("http://duckduckgo.com"))
		self.browser.urlChanged.connect(self.update_urlbar)
		self.browser.loadFinished.connect(self.update_title)
		self.setCentralWidget(self.browser)
		self.status = QStatusBar()
		self.setStatusBar(self.status) 
		navtb = QToolBar("Navigation")
		self.addToolBar(navtb)
		back_btn = QAction("<", self)
		back_btn.setStatusTip("Back")
		back_btn.triggered.connect(self.browser.back)
		navtb.addAction(back_btn)
		next_btn = QAction(">", self)
		next_btn.setStatusTip("Forward")
		next_btn.triggered.connect(self.browser.forward)
		navtb.addAction(next_btn)
		reload_btn = QAction("F5", self)
		reload_btn.setStatusTip("Reload")
		reload_btn.triggered.connect(self.browser.reload)
		navtb.addAction(reload_btn)
		home_btn = QAction("⌂", self)
		home_btn.setStatusTip("back to duckduckgo")
		home_btn.triggered.connect(self.navigate_home)
		navtb.addAction(home_btn)
		navtb.addSeparator()
		self.urlbar = QLineEdit()
		self.urlbar.returnPressed.connect(self.navigate_to_url)
		navtb.addWidget(self.urlbar)
		self.show()

	def update_title(self):
		title = self.browser.page().title()
		self.setWindowTitle("% s > Slowness" % title)

	def navigate_home(self):

		self.browser.setUrl(QUrl("http://www.duckduckgo.com"))

	def navigate_to_url(self):

		q = QUrl(self.urlbar.text())

		if q.scheme() == "":
			
			q.setScheme("http")

		self.browser.setUrl(q)

	def update_urlbar(self, q):

		self.urlbar.setText(q.toString())

		self.urlbar.setCursorPosition(0)


app = QApplication(sys.argv)
app.setApplicationName("Slowness")
# Now use a palette to switch to dark colors:
palette = QPalette()
palette.setColor(QPalette.Window, QColor(53, 53, 53))
palette.setColor(QPalette.WindowText, Qt.white)
palette.setColor(QPalette.Base, QColor(25, 25, 25))
palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
palette.setColor(QPalette.ToolTipBase, Qt.black)
palette.setColor(QPalette.ToolTipText, Qt.white)
palette.setColor(QPalette.Text, Qt.white)
palette.setColor(QPalette.Button, QColor(53, 53, 53))
palette.setColor(QPalette.ButtonText, Qt.white)
palette.setColor(QPalette.BrightText, Qt.red)
palette.setColor(QPalette.Link, QColor(42, 130, 218))
palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
palette.setColor(QPalette.HighlightedText, Qt.black)
app.setPalette(palette)
window = MainWindow()

app.exec_()